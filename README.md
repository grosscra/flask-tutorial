# Flask tutorial

These are the files created working through the [Flask tutorial](https://flask.palletsprojects.com/en/2.1.x/tutorial/).

In addition to the original tutorial, this repository also includes a CI/CD pipeline to run tests and build the resulting package for deployment.
